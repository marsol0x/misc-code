#if 0
clang -O2 -std=c89 -lpthread $0
exit
#endif
/* ==================================================================================
   $File: $
   $Date: $
   $Revision: $
   $Creator: Marshel Helsper $
   $Notice: $
   ================================================================================== */

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

typedef signed char         i8;
typedef signed short       i16;
typedef signed int         i32;
typedef signed long long   i64;
typedef unsigned char       u8;
typedef unsigned short     u16;
typedef unsigned int       u32;
typedef unsigned long long u64;

typedef float              r32;
typedef double             r64;

typedef signed int         b32;
#define true (1==1)
#define false (!true)

#define MAX_SIZE 256
typedef struct
{
    char Elements[MAX_SIZE];
    u32 ReadPos;
    u32 WritePos;
} FIFO;

void FIFOPush(FIFO *Fifo, char Element)
{
    u32 WritePos;

    // NOTE(mhelsper): Block if FIFO is full
    do
    {
        WritePos = Fifo->WritePos;
    } while (!__sync_bool_compare_and_swap(&Fifo->WritePos, WritePos, WritePos + 1));

    Fifo->Elements[Fifo->WritePos % MAX_SIZE] = Element;
}

char FIFOPop(FIFO *Fifo)
{
    char Result;
    u32 ReadPos;
    
    // NOTE(mhelsper): Block if FIFO is empty
    do
    {
        ReadPos = Fifo->ReadPos;
    } while (!__sync_bool_compare_and_swap(&Fifo->ReadPos, ReadPos, ReadPos + 1));

    Result = Fifo->Elements[ReadPos % MAX_SIZE];

    return Result;
}

u32 FIFOCount(FIFO *Fifo)
{
    __sync_synchronize();
    return Fifo->WritePos - Fifo->ReadPos;
}

typedef enum
{
    THREADSTATUS_NOTRUNNING,
    THREADSTATUS_RUNNING,
} ThreadStatus;

typedef struct
{
    ThreadStatus Status;
    char *FilePath;
    FIFO *Fifo;
} ThreadData;

void * PopulateFIFO(void *ThreadDataPointer)
{
    ThreadData *Data = (ThreadData *)ThreadDataPointer;
    FILE *File;

    File = fopen(Data->FilePath, "r");
    if (File)
    {
        char Input;
        while ((Input = fgetc(File)))
        {
            if (Input == EOF) break;
            FIFOPush(Data->Fifo, Input);
        }
        fclose(File);
    }

    Data->Status = THREADSTATUS_NOTRUNNING;

    return 0;
}

i32 main(i32 argc, char **argv)
{
    FIFO Fifo       = {0};
    ThreadData Data = {0};
    pthread_t PopulateThread;
    char Result;
    i32 i;
    i32 Error;

    if (argc != 2)
    {
        fprintf(stderr, "Invalid number of arguments.\n");
        printf("Usage:\n\t%s <file>\n", argv[0]);
        exit(2);
    }

    Data.Status   = THREADSTATUS_RUNNING;
    Data.FilePath = argv[1];
    Data.Fifo     = &Fifo;
    Error = pthread_create(&PopulateThread, 0, PopulateFIFO, (void *)&Data);
    if (Error != 0)
    {
        fprintf(stderr, "Creating thread failed with error: %d\n", Error);
        exit(1);
    }

    for (;;)
    {
        u32 Count = FIFOCount(&Fifo);
        if (Count)
        {
            char Result = FIFOPop(&Fifo);
            putchar(Result);
        }

        if (!Count && Data.Status == THREADSTATUS_NOTRUNNING)
        {
            break;
        }
    }

    return 0;

}
