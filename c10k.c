#if 0
clang -g -O0 c10k.c
exit
#endif
/* ==================================================================================
   $File: $
   $Date: $
   $Revision: $
   $Creator: Marshel Helsper $
   $Notice: (C) Copyright 2017 by Dun & Bradstreet NetProspex. All Rights Reserved. $
   ================================================================================== */
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>

#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <unistd.h>
#include <poll.h>
#include <fcntl.h>

#define false (1==0)
#define true (!false)
typedef signed int b32;
typedef signed char i8;
typedef signed short i16;
typedef signed int i32;
typedef signed long long i64;
typedef unsigned char u8;
typedef unsigned short u16;
typedef unsigned int u32;
typedef unsigned long long u64;
typedef float r32;
typedef double r64;

#include "dynamicarray.c"

void close_socket_connection(struct pollfd *fds, i32 index)
{
    close(fds[index].fd);
    dynamic_array_erase(fds, index);
}

i32 main(i32 argc, char **argv)
{
    i32 serverSocket;
    struct sockaddr_in serverAddress = {0};
    struct pollfd *fds = 0;
    struct pollfd serverPoll = {0};
    char *httpReply = "HTTP/1.1 200 OK\r\nContent-Length: 0\r\n\r\n";
    i32 httpReplyLength = strlen(httpReply);

    serverSocket  = socket(PF_INET, SOCK_STREAM, 0);
    if (serverSocket == -1)
    {
        perror("Failed to create new socket");
        exit(1);
    }

    serverAddress.sin_family = PF_INET;
    serverAddress.sin_port = htons(4000);
    serverAddress.sin_addr.s_addr = htonl(INADDR_ANY);
    if (bind(serverSocket, (struct sockaddr *)&serverAddress, sizeof(serverAddress)) != 0)
    {
        perror("Failed to bind socket");
        exit(1);
    }

    if (listen(serverSocket, 100) != 0)
    {
        perror("Failed to listen on port 4000");
        exit(1);
    }

    dynamic_array_init(fds, struct pollfd);

    serverPoll.fd = serverSocket;
    serverPoll.events = POLLIN;
    dynamic_array_push(fds, serverPoll);

    for (;;)
    {
        if (poll(fds, dynamic_array_size(fds), 0) > 0)
        {
            i32 index = 0;
            while (index < dynamic_array_size(fds))
            {
                struct pollfd p = fds[index];
                if (p.revents & POLLIN)
                {
                    if (p.fd == serverSocket)
                    {
                        // NOTE(mhelsper): Accept new connection
                        struct sockaddr_in clientAddress;
                        u32 clientAddressLength;
                        i32 clientSock = accept(serverSocket, (struct sockaddr *)&clientAddress, &clientAddressLength);
                        if (clientSock > 0)
                        {
                            struct pollfd newClient = {0};
                            if (fcntl(clientSock, F_SETFL, O_NONBLOCK) == -1)
                            {
                                perror("Failed to configure new client connection as non-blocking");
                                close(clientSock);
                            } else {
                                newClient.fd = clientSock;
                                newClient.events = POLLIN | POLLOUT;
                                dynamic_array_push(fds, newClient);
                            }
                        } else {
                            perror("Failed to establish new client connection");
                        }
                    } else {
                        if (p.revents & POLLOUT)
                        {
                            i32 bytesRead, bytesSent;
                            char buffer[1024] = {0};
                            bytesRead = read(p.fd, buffer, 1023);

                            switch (bytesRead)
                            {
                                case 0:
                                {
                                    // NOTE(mhelsper): The socket closed, do nothing
                                    close_socket_connection(fds, index);
                                } break;

                                case -1:
                                {
                                    if (errno != EAGAIN)
                                    {
                                        perror("Client socket read-error");
                                        close_socket_connection(fds, index);
                                    } else {
                                        p.revents = 0;
                                    }
                                } break;

                                default:
                                {
                                    buffer[bytesRead] = 0;

                                    bytesSent = 0;
                                    do
                                    {
                                        bytesSent += write(p.fd, httpReply, httpReplyLength - bytesSent);
                                    } while (bytesSent < httpReplyLength);
                                    close_socket_connection(fds, index);
                                    --index;
                                } break;
                            }
                        }
                    }
                } else if (p.revents & POLLHUP) {
                    // NOTE(mhelsper): Connection closed
                    close_socket_connection(fds, index);
                    --index;
                }
                ++index;
            }
        }
    }

    return 0;
}
