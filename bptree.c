#if 0
clang -O2 -std=c89 $0
exit
#endif
/* ==================================================================================
   $File: $
   $Date: $
   $Revision: $
   $Creator: Marshel Helsper $
   $Notice: $
   ================================================================================== */

#include <stdio.h>
#include <stdlib.h>

typedef signed char         i8;
typedef signed short       i16;
typedef signed int         i32;
typedef signed long long   i64;
typedef unsigned char       u8;
typedef unsigned short     u16;
typedef unsigned int       u32;
typedef unsigned long long u64;

typedef float              r32;
typedef double             r64;

typedef signed int         b32;
#define true (1==1)
#define false (!true)

i32 main(i32 argc, char **argv)
{
    printf("Hello, world!\n");

    return 0;
}
